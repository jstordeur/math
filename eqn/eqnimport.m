## Copyright (C) 2019 jstordeur
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## Author: jstordeur <stordeur.jordan@gmail.com>
## Created: 2019-04-25

##Import the functions to be able to use them in main.m
function message = eqnimport
  assignin('base','evalfunc_ode',@evalfunc_ode);
  assignin('base','func',@func);
  assignin('base','jac',@jac);
  message='Done importing eqn fuctions to workspace';
end

%=============================
% For ODE45
% Func writted by J. Walmag
%=============================

function dery = evalfunc_ode(tau,y)
    deru= -y(1)*y(2);
    derv= y(1)*y(2) - y(2);
    dery=[deru ; derv];
endfunction


%=============================
% For CN scheme
%=============================

%s is sigma at n+1 and s_ is sigma at n
%l is lambda at n+1 and l_ is lambda at n
function vec = func(s, s_, l, l_, dt)
  g = s / dt + .5 * l*s - s_/dt + .5*l_*s_;
  h = l*(1/dt + .5) - .5*s*l - l_/dt - .5*s_*l_ + .5*l_;
  
  vec = [g; h];
endfunction



% Everything is function of variable at n+1
%That makes sense
function mat = jac(s, l, dt)
    mat = [1/dt + .5*l, .5*s;...
                      -.5*l, 1/dt - .5*s + .5];
endfunction
