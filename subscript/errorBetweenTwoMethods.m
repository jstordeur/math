%ode45
	[t, y] = ode45(evalfunc_ode,tspan,CI);

%CN scheme 
	[thm, yhm] = ode45_homemade(func, jac, tspan, CI, newtonprec, verbose);

%Plot this
	figure();
	plot(tspan, abs(yhm - y'));
		title('Compute the error between de two methods');
		xlabel('dt [s]');
		ylabel('Crank Nicolson - ode45');
		legend('error on Suscpetible','Error on ill');