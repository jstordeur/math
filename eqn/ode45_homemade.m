## Copyright (C) 2019 jstordeur
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## Author: jstordeur <stordeur.jordan@gmail.com>
## Created: 2019-04-25


######################################################
######################################################
###                                                ###
### This function is hardcoded by lack of time :'( ###
###                                                ###
######################################################
######################################################


function [t, y] = ode45_homemade (func, jac, tspan, CI, newtonprec = 1e-4, verbose = 0)
 
%Mesh
  dt = tspan(2) - tspan(1);
  mesh_length = size(tspan, 2);
  pts = zeros(2, mesh_length);

%condinit
  pts(:, 1) = CI;

%Compute
for i=2:1:mesh_length
  %make it easy to read
    sigma_ = pts(1, i-1);
    lambda_ = pts(2, i-1);

  %Cranki <3
  	%I fix the parameters  that must be fixed :)
    f_subs = @(input_vec) func(input_vec(1), sigma_, input_vec(2), lambda_, dt);
    jac_subs = @(input_vec) (jac(input_vec(1), input_vec(2), dt));
    
    pts(:, i) = newton(f_subs, jac_subs, pts(:, i-1), newtonprec, verbose);
endfor

  t = tspan;
  y = pts;

endfunction
