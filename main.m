clc;
clear all;
close all;

#Add subscript and eqn folder to PATH
addpath subscript
addpath eqn

%============
% Msg box
%============
icon = "warn";
m_title = "Interrupt in the routine";
message = "Close last figure or press RET in order to go trough the code";

%============
% Parameters
%============
  CI = [10, 0.1];
  dt = 1e-2;
  tspan=[0:dt:3];
  newtonprec = 1e-10;
  verbose = 0;

%============
% Functions
%============
  eqnimport;

%============
% Computing
%============

% Let's see if it works
	plotit
	msgbox({"A dummy example :\n", message}, m_title, icon);
	ginput();

% Now what happen if we change the CI ?
 	CI = [0, 150];
 	plotit
	msgbox({"Influence of CI :\n", message}, m_title, icon);
	ginput();

	CI = [50, 1];
	close all
	plotit
	msgbox({"Influence of CI :\n", message}, m_title, icon);
	ginput();

% Influence of beta and nu parameters
	CI = [10, 0.1];
	close all
	paramInfluence
	ginput();

% What about the time computing ?
% This one will take e few minutes to run (around 1min on my PC)
% Uncomment the block (CTRL+SHIFT+R on mouse selection) to see what it gives

% close all
% msgbox("Be careful, the next Plot will be slow to compute. I got 1min on my computer", m_title, icon);
%
%	CI = [10 0.1];
%
%	time_analysis
%	msgbox({"Time analysis :\n", message}, m_title, icon);
%	ginput();

% A little word abount the difference between ODE45 and CN follow by Newton
	close all
	errorBetweenTwoMethods
	msgbox({"Error between Netwon-Raphson (CN scheme) and ODE45 :\n", message}, m_title, icon);
	ginput();

# Remove foder from PATH
rmpath subscript
rmpath eqn
