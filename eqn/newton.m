## Copyright (C) 2019 jstordeur
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## Author: jstordeur <stordeur.jordan@gmail.com>
## Created: 2019-04-25

function newpoints = newton (func, jac, condinit, precwanted=1e-4, verbose=0)
  
  %Variable used for verbose
  weirdstep = 100; %If newton has more than weirdstep iterations, it's weird !
  step = 0;
  notPrinted = 1;
  
  %init newtons' method
  oldpoints = condinit;
  
  do    
    %%%%%%%%%%%%%%
    % Instructions
    %%%%%%%%%%%%%%
    
    newpoints = oldpoints - jac(oldpoints) \ func(oldpoints);
   
    %%%%%%%%%%%%%%%
    % End condition
    %%%%%%%%%%%%%%%
    
    precvec = newpoints - oldpoints;
    precvec = abs(precvec);
    
    %%%%%%%%%%%%%%%
    % Update points
    %%%%%%%%%%%%%%%
   
    oldpoints = newpoints;
    
    %%%%%%%%%
    % Verbose
    %%%%%%%%%
    
    if(verbose)
      step = step + 1;
      if(step > weirdstep && notPrinted)
        printf("The convergence seems to be waird\n");
        notPrinted = 0; %Variable to print this line only one time
      endif
    endif
    
  until (precvec < precwanted)
  
  if(verbose)
    printf("Newton method has converged in %d steps\n", step + 1);
  endif
  
endfunction
