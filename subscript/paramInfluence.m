%msgbox({"Interrupt in the routine :", "Hit enter to go trough the plot", "Influence of beta and nu", "warn");
msgbox({"Influence of beta and nu :\n", "Hit enter to go through the plot", message}, m_title, icon);

plot_style = ['-'; '--'; ':'];

%Compute for only one CI
[t, ypts] = ode45_homemade(func, jac, tspan, CI, newtonprec, verbose);

%============
% Nu influence
%============
	nu=[1.5, 1.1, 0.6];
	beta=1.4;

	for i=1:1:length(nu)
		time=t/nu(i);
		S_I=(nu(i)/beta).*ypts;

		%Plotting
		figure(1); hold on;
		plot(time, S_I, plot_style(i, :));
			title('Nu variation for beta = 1.4');
			xlabel('time');
			ylabel('#');

		%Plot one a time
		ginput();
	endfor
	
	legend('# of Suscpetible (nu=1.5)','# of Ill (nu=1.5)','# of Suscpetible (nu=1.1)','# of Ill (nu=1.5)','# of Suscpetible (nu=0.6)','# of Ill (nu=1.5)');

%==============
% Beta influence
%==============
	ginput();

	nu=1.5;
	beta=[1.4, 0.9, 1.9];
	
	for i=1:1:length(beta)
		time=t/nu;
		S_I=(nu/beta(i)).*ypts;
		
		%Plotting
		figure(2); hold on;
		plot(time, S_I, plot_style(i, :));
			title('Beta variation for nu = 1.5');
			xlabel('time');
			ylabel('#');

		%Plot one a time
		ginput();
	endfor
	
	legend('# of Suscpetible (beta=1.4)','# of Ill (beta=1.4)','# of Suscpetible (beta=0.9)','# of Ill (beta=1.4)','# of Suscpetible (beta 1.9)','# of Ill');

%==============
% S/I constant
%==============
	ginput();

	nu=[0.5, 1.5];
	beta=[0.5, 1.5];
	
	for i=1:1:length(nu)
		time=t/nu(i);
		S_I=(nu(i)/beta(i)).*ypts;
		
		%Plotting
		figure(3); hold on;
		plot(time, S_I, plot_style(i, :));
			title('S/I variation');
			xlabel('time');
			ylabel('#');

		%Plot one a time
		ginput();
	endfor
	
	legend('# of Suscpetible(nu=beta=0.5)','# of Ill','# of Suscpetible(nu=beta=1.5)','# of Ill');