################
# Requirements #
################

In order to run the code, octave is needed.

The base installation is enough.

  ############
  # On Linux #
  ############

    On Linux, no wories : Octave is provided in your package manager. Just install it with apt/yum/pacman

  #############
  # Others OS #
  #############

    For other OS go on : https://www.gnu.org/software/octave/download.html
    Download then install it.

#########################
# A word on the program #
#########################

The program is cut in several files.

  In the subfolder eqn, you will find everything related to the equation and how to calculate the points
  
  In the subfolder subscript, you will find "parts of main script" that will do specifics stuff.

The routine of the main is also cut, mainly, between the subscript.

  At each change of subscript a msgbox will be displayed. This one is only for information.

  To continue the script you will have to close the last figure or hit the return key (while the last figure in on focus. It's important !)

The message box allow you to not have every figure all in one. There is maybe to many msgbox but I wanted it to be clear :)

#####################
# Launch the script #
#####################

As the name tells us, you need to run the main.m to be able to see our work.

###############
# Other words #
###############

I'm still joinable at stordeur.jordan@gmail.com if there is some darks errors (After all Octave is still in developpement !)

Enjoy :)
