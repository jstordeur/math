%
%Ode45
%============
  [taunum,ynum] = ode45(evalfunc_ode,tspan,CI);

%Plotting
  figure(1);
    plot(taunum,ynum,'o-');
	title("ODE45")
	xlabel('Time');
    ylabel('# People');
    legend('# of Suscpetible','# of Ill');

  figure(2);
    plot(ynum(:,2),ynum(:,1));
    title('Plan de phase');
    xlabel('# Ill');
    ylabel('# Suceptible');

%
%Newton
%============
  [t, ypts] = ode45_homemade(func, jac, tspan, CI, newtonprec, verbose);

%Plotting
figure(3);
  plot(t, ypts, '-o');
  title("ODE45 Homemade")
  xlabel('Time');
  ylabel('# People');
  legend('# of Suscpetible','# of Ill');