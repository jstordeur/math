%============
% Computing
%============
  
  vec = 1:1:200;
  for i = vec
    disp(i);
    tspan = [0:dt/i:3];
    
    tic;
    [t, y] = ode45(evalfunc_ode,tspan,CI);
    elapsed_ode45(i) = toc;
    
    tic;
    ode45_homemade(func, jac, tspan, CI, newtonprec, verbose);
    elapsed_ode45_homemade(i) = toc;
  endfor

  figure();
  loglog(dt ./ vec, elapsed_ode45_homemade./elapsed_ode45);
    title('Compute time of homemade function vs octave ine over the dt');
    xlabel('dt [s]');
    ylabel('time HM / ODE45');